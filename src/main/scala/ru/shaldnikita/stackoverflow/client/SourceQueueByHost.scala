package ru.shaldnikita.stackoverflow.client

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicReference

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.stream.OverflowStrategy
import akka.stream.scaladsl.{
  Keep,
  RetryFlow,
  Sink,
  Source,
  SourceQueueWithComplete
}

import scala.collection.mutable
import scala.concurrent.Promise
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}

object SourceQueueByHost {

  private val map = mutable
    .HashMap[String,
             SourceQueueWithComplete[(HttpRequest, Promise[HttpResponse])]]()

  def getQueueForHost(
      host: String,
      port: Int,
      maxConnections: Int
  )(implicit as: ActorSystem)
    : SourceQueueWithComplete[(HttpRequest, Promise[HttpResponse])] = {
    val currentQueue = map.get(host)
    currentQueue match {
      case Some(queue) => queue
      case None =>
        // Можно использовать atomicReference/concurrentHashMap для большей эффективности
        map.synchronized {
          val currentQueue = map.get(host)
          currentQueue match {
            case Some(queue) => queue
            case None =>
              val queue = createQueue(host, port, maxConnections)
              map.put(host, queue)
              queue
          }
        }
    }
  }

  private def createQueue(host: String, port: Int, maxConnections: Int)(
      implicit as: ActorSystem)
    : SourceQueueWithComplete[(HttpRequest, Promise[HttpResponse])] = {

    val cachedPool = {
      if (port == 443) {
        Http().cachedHostConnectionPoolHttps[Promise[HttpResponse]](host, port)
      } else {
        Http().cachedHostConnectionPool[Promise[HttpResponse]](host, port)
      }

    }
    val retryingPool =
      RetryFlow.withBackoff(10.millis, 1.seconds, 0d, 3, cachedPool) {
        case (in @ (req, reqPromise), (resp, respPromise)) =>
          if (resp.isFailure || resp.get.status.isFailure()) Some(in) else None
      }
    Source
      .queue[(HttpRequest, Promise[HttpResponse])](
        maxConnections,
        OverflowStrategy.backpressure,
        Int.MaxValue) // Можно вынести в проперти и поменять
      .via(retryingPool)
      .async
      .toMat(Sink.foreach({
        case (Success(resp), p) => p.success(resp)
        case (Failure(e), p)    => p.failure(e)
      }))(Keep.left)
      .async
      .run
  }

}
