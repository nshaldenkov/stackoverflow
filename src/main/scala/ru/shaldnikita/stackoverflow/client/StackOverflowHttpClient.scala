package ru.shaldnikita.stackoverflow.client

import akka.NotUsed
import akka.actor.ActorSystem
import akka.http.scaladsl.coding.Coders
import akka.http.scaladsl.common.{EntityStreamingSupport, JsonEntityStreamingSupport}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.unmarshalling.{Unmarshal, Unmarshaller}
import akka.stream.scaladsl.{Sink, Source, SourceQueueWithComplete}
import akka.stream.{Materializer, QueueOfferResult}
import ru.shaldnikita.stackoverflow.client.StackOverflowHttpClient._
import ru.shaldnikita.stackoverflow.client.model.{ItemStats, StackOverflowRequestException, StackOverflowResponse}

import scala.concurrent.{ExecutionContext, Future, Promise}

class StackOverflowHttpClient(
    queue: SourceQueueWithComplete[(HttpRequest, Promise[HttpResponse])])(
    implicit as: ActorSystem,
    ec: ExecutionContext,
    mat: Materializer) {

  def getStatsByTags(tags: Set[String]): Future[List[ItemStats]] = {
    Source(tags)
      .mapAsync(tags.size)(putTagToRequestsQueue)
      .map(decode)
      .mapAsync(tags.size)(handleResponse)
      .flatMapConcat(identity)
      .mapConcat(_.items)
      .runWith(Sink.collection)
  }

  private def putTagToRequestsQueue(tag: String) = {
    val promise = Promise[HttpResponse]
    val request = HttpRequest(uri = buildUri(tag)) -> promise
    queue.offer(request).flatMap {
      case QueueOfferResult.Enqueued => promise.future
      case QueueOfferResult.Dropped =>
        Future.failed(
          new RuntimeException("Queue overflowed. Try again later."))
      case QueueOfferResult.Failure(ex) => Future.failed(ex)
      case QueueOfferResult.QueueClosed =>
        Future.failed(new RuntimeException(
          "Queue was closed (pool shut down) while running the request. Try again later."))
    }
  }

}
object StackOverflowHttpClient {
  val PageSize = 100
  val SortOrder = "desc"
  val Sort = "creation"
  val Site = "stackoverflow"

  private def buildUri(tag: String): String = {
    "/2.2/search?" +
      s"pagesize=$PageSize&" +
      s"order=$SortOrder&" +
      s"sort=$Sort&" +
      s"site=$Site" +
      s"&tagged=$tag"
  }

  private def decode(response: HttpResponse) = {
    Coders.Gzip.decodeMessage(response)
  }

  implicit private val jsonStreamingSupport: JsonEntityStreamingSupport =
    EntityStreamingSupport.json(100000)

  def handleResponse(response: HttpResponse)(
      implicit
      materializer: Materializer,
      ec: ExecutionContext): Future[Source[StackOverflowResponse, NotUsed]] = {
    if (response.status.isSuccess()) {
      Unmarshal(response.entity.withoutSizeLimit())
        .to[Source[StackOverflowResponse, NotUsed]]
    } else {
      Unmarshaller.stringUnmarshaller(response.entity).flatMap {
        responseString =>
          Future.failed(StackOverflowRequestException(responseString))
      }
    }
  }
}
