package ru.shaldnikita.stackoverflow.client.model

import spray.json.DefaultJsonProtocol._
import spray.json.RootJsonFormat

case class StackOverflowResponse(items: List[ItemStats])

case class ItemStats(tags: List[String], isAnswered: Boolean)

object ItemStats {
  implicit val itemStatsFormat =
    jsonFormat(ItemStats.apply, "tags", "is_answered")
}

object StackOverflowResponse {
  implicit val stackOverflowResponseFormat
    : RootJsonFormat[StackOverflowResponse] = jsonFormat1(
    StackOverflowResponse.apply)
}
