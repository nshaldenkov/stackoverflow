package ru.shaldnikita.stackoverflow.client.model

case class StackOverflowRequestException(msg: String)
    extends RuntimeException(msg)
