package ru.shaldnikita.stackoverflow

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import com.typesafe.config.ConfigFactory
import ru.shaldnikita.stackoverflow.client.{
  SourceQueueByHost,
  StackOverflowHttpClient
}
import ru.shaldnikita.stackoverflow.service.StackOverflowService
import ru.shaldnikita.stackoverflow.web.Handler

object Main extends App {

  implicit val ac = ActorSystem("app")
  implicit val ec = ac.dispatcher

  val conf = ConfigFactory.defaultApplication
  val maxConnections =
    conf.getInt("akka.http.host-connection-pool.max-open-requests")

  val stackOverflowHttpClient = {
    val queue = SourceQueueByHost.getQueueForHost("api.stackexchange.com",
                                                  443,
                                                  maxConnections)
    new StackOverflowHttpClient(queue)
  }

  val stackOverflowService = new StackOverflowService(stackOverflowHttpClient)
  val route = new Handler(stackOverflowService)

  Http().newServerAt("localhost", 8080).bind(route.route)

}
