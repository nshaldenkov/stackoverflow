package ru.shaldnikita.stackoverflow.service
import ru.shaldnikita.stackoverflow.client.StackOverflowHttpClient
import ru.shaldnikita.stackoverflow.client.model.ItemStats
import ru.shaldnikita.stackoverflow.service.StackOverflowService.{
  TotalTagsStats,
  groupResponses
}

import scala.concurrent.{ExecutionContext, Future}

class StackOverflowService(stackOverflowClient: StackOverflowHttpClient)(
    implicit ec: ExecutionContext) {

  def getTopicsByTags(tags: Iterable[String]): Future[TotalTagsStats] = {
    val uniqueTags = tags.toSet
    stackOverflowClient
      .getStatsByTags(uniqueTags)
      .map(groupResponses)
  }

}
object StackOverflowService {

  private def groupResponses(items: List[ItemStats]): TotalTagsStats = {
    val tagsStats = items
      .foldLeft(Map[String, SingleTagStats]()) { (acc, item) =>
        var newAcc = acc
        item.tags.foreach { tag =>
          val existingStats = acc.get(tag)

          val newStats = existingStats match {
            case None =>
              SingleTagStats(
                tag,
                1,
                if (item.isAnswered) 1 else 0
              )

            case Some(SingleTagStats(tag, total, answered)) =>
              val newTotal = total + 1
              val newAnswered = if (item.isAnswered) answered + 1 else answered
              SingleTagStats(tag, newTotal, newAnswered)
          }

          newAcc = newAcc.updated(tag, newStats)
        }

        newAcc
      }
      .values
      .toList

    TotalTagsStats(tagsStats)

  }

  case class TotalTagsStats(tagCounters: List[SingleTagStats])

  case class SingleTagStats(tag: String, total: Int, answered: Int)
}
