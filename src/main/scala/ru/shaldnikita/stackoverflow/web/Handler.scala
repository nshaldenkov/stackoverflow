package ru.shaldnikita.stackoverflow.web

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import ru.shaldnikita.stackoverflow.service.StackOverflowService
import ru.shaldnikita.stackoverflow.web.model.TotalTagsStatsViewMapper
import spray.json.PrettyPrinter

import scala.concurrent.ExecutionContext

class Handler(stackOverflowService: StackOverflowService)(
    implicit ec: ExecutionContext) {

  implicit val printer = PrettyPrinter
  def route: Route =
    (path("search") & pathEndOrSingleSlash & get &
      parameter("tag".as[String].*)) { tags =>
      val result = stackOverflowService
        .getTopicsByTags(tags)
        .map(totalTagsCounter =>
          TotalTagsStatsViewMapper.mapToView(totalTagsCounter))

      complete(result)
    }
}
