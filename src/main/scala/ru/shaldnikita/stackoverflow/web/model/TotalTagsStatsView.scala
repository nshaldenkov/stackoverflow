package ru.shaldnikita.stackoverflow.web.model

import spray.json.DefaultJsonProtocol._

final case class TagStatsView(tag: String, total: Int, answered: Int)

object TagStatsView {
  implicit val tagStatsViewFormat = jsonFormat3(TagStatsView.apply)
}

final case class TotalTagsStatsView(tagsCounters: List[TagStatsView])

object TotalTagsStatsView {
  implicit val totalTagsStatsViewFormat = jsonFormat1(TotalTagsStatsView.apply)
}
