package ru.shaldnikita.stackoverflow.web.model

import ru.shaldnikita.stackoverflow.service.StackOverflowService.TotalTagsStats

object TotalTagsStatsViewMapper {
  def mapToView(totalCounter: TotalTagsStats): TotalTagsStatsView = {
    val mappedTagsCounters = totalCounter.tagCounters.map(totalCounter =>
      TagStatsView(totalCounter.tag, totalCounter.total, totalCounter.answered))
    TotalTagsStatsView(mappedTagsCounters)
  }
}
