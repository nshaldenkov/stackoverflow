package ru.shaldnikita.stackoverflow.service

import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import ru.shaldnikita.stackoverflow.client.StackOverflowHttpClient
import ru.shaldnikita.stackoverflow.client.model.ItemStats
import ru.shaldnikita.stackoverflow.service.StackOverflowService.SingleTagStats

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class StackOverflowServiceSpec
    extends AnyFlatSpec
    with Matchers
    with MockFactory
    with ScalaFutures {

  val client = mock[StackOverflowHttpClient]
  val service = new StackOverflowService(client)

  "StackOverflowService" should "successfully group stats by tags" in {

    (client.getStatsByTags _)
      .expects(Set("java", "scala"))
      .returning(
        Future.successful(
          List(
            ItemStats(List("scala", "tests"), isAnswered = true),
            ItemStats(List("scala", "java"), isAnswered = true),
            ItemStats(List("scala", "java", "sql"), isAnswered = false),
            ItemStats(List("scala", "java", "sql", "tests"),
                      isAnswered = false),
          )
        ))

    service
      .getTopicsByTags(Seq("java", "scala"))
      .futureValue
      .tagCounters should contain

    List(
      SingleTagStats("scala", total = 4, answered = 2),
      SingleTagStats("java", total = 3, answered = 1),
      SingleTagStats("sql", total = 2, answered = 0),
      SingleTagStats("tests", total = 2, answered = 1)
    )

  }

}
