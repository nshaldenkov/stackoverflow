package ru.shaldnikita.stackoverflow.client

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, StatusCodes}
import akka.http.scaladsl.server.Directives._
import akka.stream.{BufferOverflowException, OverflowStrategy}
import akka.stream.scaladsl.{Keep, RetryFlow, Sink, Source}
import com.typesafe.config.ConfigFactory
import org.scalamock.scalatest.MockFactory
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Seconds, Span}
import ru.shaldnikita.stackoverflow.Main.maxConnections
import ru.shaldnikita.stackoverflow.client.model.{ItemStats, StackOverflowResponse}

import scala.concurrent.duration._
import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success}
/**
 * {"error_id":502,"error_message":"too many requests from this IP, more requests available
 * in 85024 seconds","error_name":"throttle_violation"}
 *
 * Аккуратнее с тестами :)
 *
 * Из-за ошибки выше буду дергать локальную апишку.
 *
 */
class StackOverflowHttpClientSpec extends AnyFlatSpec
  with Matchers
  with MockFactory
  with ScalaFutures
  with BeforeAndAfterAll{


  val conf = ConfigFactory.parseResources("application-test.conf")
  implicit val ac = ActorSystem("test", ConfigFactory.parseResources("application-test.conf"))
  implicit val ec = ac.dispatcher

  val maxConnections = conf.getInt("akka.http.host-connection-pool.max-open-requests")

  val realClient = {
    val queue = SourceQueueByHost.getQueueForHost(
      "api.stackexchange.com",
      443,
      maxConnections)
    new StackOverflowHttpClient(queue)
  }

  val localClient = {
    val queue = SourceQueueByHost.getQueueForHost(
      "localhost",
      8080,
      maxConnections)
    new StackOverflowHttpClient(queue)
  }

  implicit val timeout: PatienceConfig = PatienceConfig(Span(10, Seconds),Span(100, Millis))

  "StackOverflowHttpClient" should "not fail on many elements" in {
    val tags = (1 to 200).map(_.toString).toSet

    val result = realClient.getStatsByTags(tags)
      .futureValue

    result.nonEmpty shouldBe true
  }


  "StackOverflowHttpClient" should "dont fail on many elements" in {

    val tags = (1 to 200).map(_.toString).toSet

    val result: Int = usingMockServer(failOnTag = None) {
      Future.traverse((1 to 10).toSet) { tag =>
        localClient.getStatsByTags(tags)
      }.futureValue
    }

    result shouldBe tags.size * 10
  }


  "StackOverflowHttpClient" should "retry if response code is not 200" in {
    val tags = (1 to 200).map(_.toString).toSet

    val result: Int = usingMockServer(failOnTag = None) {
      Future.traverse((1 to 10).toSet) { i =>
        localClient.getStatsByTags(tags)
      }.futureValue
    }

    result shouldBe tags.size * 10
  }


  /**
   * returns received tags
   */
  private def usingMockServer(failOnTag: Option[String])(f: => Unit): Int = {
    val tagsReceived = scala.collection.mutable.ListBuffer[String]()
    var failedOnTag = false
    val server = Http().newServerAt("localhost", 8080).bind(
      (path("2.2" / "search") & get & parameter("tagged".as[String])) { tag =>
        if(failOnTag.contains(tag) && !failedOnTag) {
          failedOnTag = true
          complete(StatusCodes.InternalServerError)
        } else {

        tagsReceived += tag

        println(s"tag -> $tag __" + Thread.currentThread().getId)
        complete(StackOverflowResponse(
          List(
            ItemStats(
              List("scala", "java"),
              true
            )
          )
        ))
      }
        }
    ).futureValue
    try f
    finally server.terminate(10.seconds).futureValue
    tagsReceived.size
  }


}
