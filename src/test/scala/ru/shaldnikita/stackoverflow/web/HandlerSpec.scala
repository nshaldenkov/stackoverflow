package ru.shaldnikita.stackoverflow.web

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import akka.http.scaladsl.model.HttpMethods.GET
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.StatusCodes.OK
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalamock.scalatest.MockFactory
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import ru.shaldnikita.stackoverflow.service.StackOverflowService
import ru.shaldnikita.stackoverflow.service.StackOverflowService.{SingleTagStats, TotalTagsStats}
import ru.shaldnikita.stackoverflow.web.model.TotalTagsStatsView._
import ru.shaldnikita.stackoverflow.web.model.{TagStatsView, TotalTagsStatsView}

import scala.concurrent.Future

class HandlerSpec
    extends AnyFlatSpec
    with Matchers
    with ScalatestRouteTest
    with MockFactory {

  val service = mock[StackOverflowService]
  val handler = new Handler(service)

  "Handler" should "response 200" in {

    (service.getTopicsByTags _)
      .expects(Seq("java", "scala"))
      .returning(
        Future.successful(
          TotalTagsStats(
            List(
              SingleTagStats("scala", 1, 1),
              SingleTagStats("java", 1, 1),
            )
          )
        ))

    HttpRequest(GET, "/search?tag=scala&tag=java") ~> Route.seal(handler.route) ~> check {
      status shouldBe OK
      responseAs[TotalTagsStatsView].tagsCounters should contain
        List(
          TagStatsView("scala", 1, 1),
          TagStatsView("java", 1, 1),
        )
    }
  }

}
